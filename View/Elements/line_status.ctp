<?php
$data = ClassRegistry::init('Tfl.TflServiceUpdate')->getLineStatusList();
$dl = array();
foreach ($data as $line => $status) {
	$url = sprintf(
		'http://www.tfl.gov.uk/tfl/livetravelnews/realtime/tube/default.html#%s',
		strtolower(Inflector::slug($line, '-'))
	);
	$options = array('target' => '_blank');
	$dl[] = implode('', array(
		$this->Html->tag(
			'dt', $this->Html->link($line, $url, $options), 
			array('class' => strtolower(Inflector::slug($line, '-')))
		),
		$this->Html->tag('dd', $this->Html->link($status, $url, $options))
	));
}
$dl = $this->Html->tag(
	'dl', implode('', $dl), 
	array('class' => 'tfl-line-status clearfix')
);
echo $dl;
