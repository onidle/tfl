<?php
App::uses('Xml', 'Utility');
App::uses('Hash', 'Utility');
App::uses('AppModel', 'App.Model');
class TflServiceUpdate extends AppModel {
	
	var $useTable = false;

	public function getLineStatus() {
		$cacheKey = sprintf('%s.getLineStatus', $this->alias);
		$results = Cache::read($cacheKey);
		if ($results !== false) {
			return $results;
		}
		$xmlUrl = 'http://cloud.tfl.gov.uk/TrackerNet/LineStatus';
		$results = Xml::toArray(Xml::build($xmlUrl));
		Cache::write($cacheKey, $results);
		return $results;
	}

	public function getLineStatusList() {
		$results = Hash::combine(
			$this->getLineStatus(), 
			'ArrayOfLineStatus.LineStatus.{n}.Line.@Name',
			'ArrayOfLineStatus.LineStatus.{n}.Status.@Description'
		);
		return $results;
	}
}
