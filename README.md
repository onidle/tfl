CakePHP Tfl Plugin
=========================
Using transport for london feeds to get info into website/cakephp elements

Requirements
------------
[CakePHP v2.x](https://github.com/cakephp/cakephp)   

How to use it
-------------
1.	Install this plugin for your CakePHP app.   
	Assuming `APP` is the directory where your CakePHP app resides, it's usually `app/` from the base of CakePHP.

		:::bash
		cd APP/Plugin   
		git clone git@bitbucket.org:onidle/tfl.git Tfl

2.  Use line status element as example

		:::php
		<?php
		// in view  
		$this->element('Tfl.line_status');
